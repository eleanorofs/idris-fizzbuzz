# A FizzBuzz Kata for Idris

This is a library which exposes two functions: 

* `Division.divides` returns a boolean to check if the first number evenly
divides the second number. (The function is defined at zero such that a zero
divisor returns false; zero does not evenly divide any number.)

* `FizzBuzz.numberToFBNString` returns a string representation of the number 
according to the standard requirements of FizzBuzz. 

## Compilation 

```sh
idris2 --build fizzbuzz.ipkg
```

## Usage 

The following Idris program prints numbers according to the requirements of 
FizzBuzz from 1 to 1000. 

```idr
module Main

import Data.Vect
import FizzBuzz

printFizzBuzz : Vect n Nat -> IO ()
printFizzBuzz Nil = putStrLn ""
printFizzBuzz (x :: xs) = 
  do putStrLn $ FizzBuzz.numberToFBNString x
     printFizzBuzz xs

total
main : IO ()
main = printFizzBuzz $ fromList [ 1 .. 1000 ]
```

## Further Reading

This project is the outcome of a blog series on the basics of Idris which 
begins with [Part I: Monads, Comments, and assert_smaller](https://webbureaucrat.gitlab.io/articles/idris-fizzbuzz-part-i-monads-comments-assert-smaller/). 
