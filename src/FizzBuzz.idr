module FizzBuzz

import Division --imports the `divides` function

data FizzBuzzNumber = ||| Represents a number divisible by both 3 and 5.
                      FizzBuzz 
                    | ||| Represents a number divisible by 3. 
                      Fizz 
                    | ||| Represents a number divisible by 5.
                      Buzz 
                    | ||| Represents a number divisible by neither 3 nor 5. 
                      Number Nat

||| Given a number, returns a Fizzbuzz representation of the number depending
||| on whether it is divisible by three, five, or both, using the `divides` 
||| function from the `Division` module.
total
classify : Nat -> FizzBuzzNumber
classify n = 
  if 15 `divides` n 
  then FizzBuzz
  else if 3 `divides` n 
       then Fizz 
       else if 5 `divides` n then Buzz else Number n 

||| Converts a `FizzBuzzNumber` value to a `String`. 
total
fbnToString : FizzBuzzNumber -> String
fbnToString FizzBuzz = "FizzBuzz"
fbnToString Fizz = "Fizz"
fbnToString Buzz = "Buzz"
fbnToString (Number n) = show n 

||| Converts a `Nat` to a `String` according to the standard requirements of
||| FizzBuzz, displaying "Fizz" if the number is divisible by 3, "Buzz" if the
||| number is divisible by 5, or "Fizzbuzz" if the number is divisible by both
||| 3 and five. 
export total
numberToFBNString : Nat -> String
numberToFBNString n = fbnToString $ classify n 
