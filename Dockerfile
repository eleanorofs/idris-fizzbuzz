from nixos/nix:master
copy src/ /home/src/
copy test/ /home/test/
copy test.sh /home/
copy test.ipkg /home/
workdir /home/
run ls
run ./test.sh
