module Main

import IdrisTest.TestResult
import TestDivision
import TestFizzBuzz

total 
tests : List IdrisTest.TestResult.TestResult
tests = TestDivision.divisionTests ++ TestFizzBuzz.fizzBuzzTests

total
main : IO ()
main = putStrLn $ IdrisTest.TestResult.toJsons tests
