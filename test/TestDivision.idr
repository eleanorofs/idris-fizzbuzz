module TestDivision

import Division
import IdrisTest.TestResult

total
testDividesWithZero : IdrisTest.TestResult.TestResult
testDividesWithZero = 
  if Division.divides 0 5
  then Fail "Division" "divides" "0 divisor should not evenly divide 5."
  else Pass "Division" "divides" "0 divisor does not evenly divide 5."

total
testDividesWithZeroes : IdrisTest.TestResult.TestResult
testDividesWithZeroes = 
  if Division.divides 0 0
  then Fail "Division" "divides" "0 divisor should not evenly divide 0."
  else Pass "Division" "divides" "0 divisor does not evenly divide 0."


total
testDividesWith1 : IdrisTest.TestResult.TestResult
testDividesWith1 =
  if Division.divides 1 20
  then Pass "Division" "divides" "1 divisor always evenly divides."
  else Fail "Division" "divides" "1 divisor should always evenly divide."

total
testDividesWithSelf : IdrisTest.TestResult.TestResult
testDividesWithSelf =
  if Division.divides 42 42
  then Pass "Division" "divides" "Self evenly divides self."
  else Fail "Division" "divides" "Self should evenly divide self."

export total
divisionTests : List IdrisTest.TestResult.TestResult
divisionTests =
  [ testDividesWithZero, testDividesWithZeroes, 
    testDividesWith1, testDividesWithSelf 
  ] 
