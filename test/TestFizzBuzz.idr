module TestFizzBuzz

import FizzBuzz
import IdrisTest.TestResult

total
testNumberToFBNStringWithZero : IdrisTest.TestResult.TestResult
testNumberToFBNStringWithZero = 
  if FizzBuzz.numberToFBNString 0 == "FizzBuzz"
  then Pass "FizzBuzz" "numberToFBNString" "0 is always evenly dividied."
  else Fail "FizzBuzz" "numberToFBNString" ("Got " ++ numberToFBNString 0)
  
total
testNumberToFBNStringWith1 : IdrisTest.TestResult.TestResult
testNumberToFBNStringWith1 = 
  if FizzBuzz.numberToFBNString 1 == "1"
  then Pass "FizzBuzz" "numberToFBNString" "1 == 1"
  else Fail "FizzBuzz" "numberToFBNString" ("1 != " ++ numberToFBNString 1)

export total
fizzBuzzTests : List IdrisTest.TestResult.TestResult
fizzBuzzTests = [ testNumberToFBNStringWithZero, testNumberToFBNStringWith1 ] 
